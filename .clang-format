---
Language: Cpp

# Change this to true to disable automatic formatting
DisableFormat: false

# Based on... but then we manually define everything...
BasedOnStyle: LLVM

# Indentation rules
IndentWidth: 4
UseTab: Never
ColumnLimit: 0
IndentGotoLabels: false
IndentCaseLabels: false
IndentCaseBlocks: true
NamespaceIndentation: None
IndentExternBlock: Indent
ContinuationIndentWidth: 2

# Alignment rules
AlignAfterOpenBracket: false
AlignConsecutiveAssignments: false
AlignConsecutiveDeclarations: false
AlignConsecutiveMacros: true
AlignEscapedNewlines: Left
AlignOperands: Align

# Short/Code 'compression' rules
AllowShortBlocksOnASingleLine: Always
AllowShortCaseLabelsOnASingleLine: false
AllowShortEnumsOnASingleLine: false
AllowShortFunctionsOnASingleLine: Empty
AllowShortIfStatementsOnASingleLine: Never
# Clang-format does not change braces content,
# as such you need to manually add braces to all statements,
# even though it will then tidy those to match the standard
AllowShortLambdasOnASingleLine: All
AllowShortLoopsOnASingleLine: false
AlwaysBreakAfterReturnType: None
AlwaysBreakBeforeMultilineStrings: false

# Braces (see indentation, too):
#BreakBeforeBraces: Allman # <- Always break before braces
#BreakBeforeBraces: Attach # <- Always attach braces to surrounding context
#BreakBeforeBraces: GNU # <- Always break before braces and add an extra level of indentation to braces of control statements, not to those of class, function or other definitions
#BreakBeforeBraces: Linux # <- Like attach, but break before braces on enum, function, and record definitions
#BreakBeforeBraces: Mozilla # <- Like Attach, but break before braces on enum, function, and record definitions
#BreakBeforeBraces: Stroustrup # <- Like Attach, but break before function definitions, and ‘else’
#BreakBeforeBraces: WebKit # <- Like Attach, but break before functions
#BreakBeforeBraces: Whitesmiths # <- Like Allman but always indent braces and line up code with braces
BreakBeforeBraces: Custom # <- Define your own
BraceWrapping:
  AfterCaseLabel: true
  AfterClass: true
  AfterEnum: true
  AfterControlStatement: Always
  AfterFunction: true
  AfterNamespace: false
  AfterStruct: false
  AfterUnion: false
  AfterExternBlock: true
  BeforeCatch: true
  BeforeElse: true
  BeforeLambdaBody: false
  BeforeWhile: false
  SplitEmptyFunction: false
  SplitEmptyRecord: false
  SplitEmptyNamespace: false
  IndentBraces: false

# Format braced lists the same as a function call in it's place (e.g. "int[3]{1, 2, 3};" and not "int[3]{ 1, 2, 3 };" )
Cpp11BracedListStyle: true

# Pointer '*' styling: e.g. void* const* x = NULL;
DerivePointerAlignment: false
PointerAlignment: Left

# Include and comment reformatting
ReflowComments: true
SortIncludes: false
SortUsingDeclarations: true
# IncludeBlocks: false

# White space rules:
KeepEmptyLinesAtTheStartOfBlocks: false
MaxEmptyLinesToKeep: 1
SpaceAfterCStyleCast: false
SpaceAfterLogicalNot: false
SpaceAfterTemplateKeyword: true
SpaceBeforeAssignmentOperators: true
SpaceBeforeCpp11BracedList: true
SpaceBeforeCtorInitializerColon: false
SpaceBeforeInheritanceColon: false
SpaceBeforeParens: ControlStatementsExceptForEachMacros
SpaceBeforeRangeBasedForLoopColon: true
SpaceBeforeSquareBrackets: false
SpaceInEmptyBlock: true
SpacesBeforeTrailingComments: 2
SpacesInSquareBrackets: false
SpacesInParentheses: false
SpacesInContainerLiterals: true
SpacesInCStyleCastParentheses: false
SpacesInAngles: false
SpaceInEmptyParentheses: false
DeriveLineEnding: false
UseCRLF: false

# Changes For Clang-format 13+:
#EmptyLineAfterAccessModifier: Never
#SortIncludes: CaseInsensitive
#IndentAccessModifiers: false
#AlignConsecutiveMacros: AcrossEmptyLinesAndComments
#AlignConsecutiveAssignments: None
#AlignConsecutiveDeclarations: None
#EmptyLineBeforeAccessModifier: LogicalBlock
#SpaceAroundPointerQualifiers: Default
#SpaceBeforeCaseColon: false

---

