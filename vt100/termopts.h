/*
BSD 3-Clause License

Copyright (c) 2018-2021, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#if !H_TERMOPTS
#define H_TERMOPTS 1

// ~~~~ TERMINAL SERIAL CONNECTION SETTINGS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Terminal speed
#define TERMBAUD (9600)

// Which device to use
#define TERMDEVICE Serial

// Size of serial buffer in bytes
#define SERIAL_BUFFER (350)  // SERIAL_BUFFER_SIZE  // (350)

// Size of buffer to be full before asserting RTS
#define RTS_ASSERT (SERIAL_BUFFER - 20)

// Size of buffer to be full before unasserting RTS
#define RTS_DEASSERT (0)

// ~~~~ TERMINAL GRID ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Number of horizontal characters
#define TERMWIDTH (44)

// Number of vertical characters
#define TERMHEIGHT (28)

// Total number of characters
#define TERMTOTAL (TERMWIDTH * TERMHEIGHT)

// How many character widths in a tab
#define TABWIDTH (4)

// How many lines to scroll by during jump scrolls (clear and redraw with line shifted)
#define TERMSCROLL (10)  // if 1, then use display driver scrolling

#define TERMWRAP (true)  // wrap the line when the max column is filled

// ~~~~ TERMINAL COMMANDS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Follow VT52 Command codes by default
#define VT52_CMD (false)

// Follow ANSI or VT52 Command codes
#define ANSI_CMD (true)

#define TERMIDENT_VT52 "\033/Z"      // reply to VT52 identify command (VT52 via compatibility mode)
#define TERMIDENT_ANSI "\033[?1;0c"  // reply to ANSI identify command (VT100 with no bells or whistles)

// ~~~~ TERMINAL CURSOR SETTINGS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Cursor character to print, if _NUL then disable, if 0xFF then do rectangle, if char, print the char
#define CURCHAR  _CBLK
#define _COFF    (0x00)  // Cursor off
#define _CRECT   (0xFF)  // Cursor block as draw rectangle
#define _CBLK    (0x80)  // Cursor block as character
#define _CUND    (0x81)  // Cursor underline as character
#define CURBLINK (1200)  // blink time in ms

// ~~~~ TERMINAL BELL HANDLING

#include "pitches.h"

#define SCREEN_BELL (false)  // Flash screen on BEL
#define LED_BELL    (false)  // Flash LED on BEL
#define AUDIO_BELL  (false)  // Beep on BEL
#define BELL_TIME   (20)
#define BELL_TONE   NOTE_A4

// ~~~~ TERMINAL CHARACTER HANDLING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// Remapping
#define DEL_DOES_BS      (false)  // convert any 0x7F, ASCII DEL characters into ASCII BS 0x08 characters
#define REMAP_WITH_TABLE (false)  // use the table at the bottom of this header to do look up of ascii characters by value instead of passing value straight through
#define UCASE            (false)  // Force all characters to be printed as uppercase, does not change VT52/ANSI commands

// Newline symbol to follow
#define _NL (_LF)  // Only changes behaviour if set to _CR

// ASCII Control Characters
#define _NUL  (0x00)
#define _SOH  (0x01)
#define _STX  (0x02)
#define _ETX  (0x03)
#define _EOT  (0x04)
#define _ENQ  (0x05)
#define _ACK  (0x06)
#define _BEL  (0x07)
#define _BS   (0x08)
#define _HT   (0x09)
#define _LF   (0x0A)
#define _VT   (0x0B)
#define _FF   (0x0C)
#define _CR   (0x0D)
#define _SO   (0x0E)
#define _SI   (0x0F)
#define _DLE  (0x10)
#define _DC1  (0x11)
#define _DC2  (0x12)
#define _DC3  (0x13)
#define _DC4  (0x14)
#define _NAK  (0x15)
#define _SYN  (0x16)
#define _ETB  (0x17)
#define _CAN  (0x18)
#define _EM   (0x19)
#define _EOF  (0x1A)
#define _SUB  (0x1A)
#define _ESC  (0x1B)
#define _FS   (0x1C)
#define _GS   (0x1D)
#define _RS   (0x1E)
#define _US   (0x1F)
#define _DEL  (0x7F)
#define _CRLF (0x0D0A)  // assumes little-endian - NOT USED

#if REMAP_WITH_TABLE
// Use these settings to remap ascii characters into different ascii characters
static const char ascii_chart[128] PROGMEM =
  {
    //     Defaults:
    _NUL,  // NUL
    _SOH,  // SOH
    _STX,  // STX
    _ETX,  // ETX
    _EOT,  // EOT
    _ENQ,  // ENQ
    _ACK,  // ACK
    _BEL,  // BEL
    _BS,   // BS
    _HT,   // HT
    _LF,   // LF
    _VT,   // VT
    _FF,   // FF
    _CR,   // CR
    _SO,   // SO
    _SI,   // SI
    _DLE,  // DLE
    _DC1,  // DC1
    _DC2,  // DC2
    _DC3,  // DC3
    _DC4,  // DC4
    _NAK,  // NAK
    _SYN,  // SYN
    _ETB,  // ETB
    _CAN,  // CAN
    _EM,   // EM
    _SUB,  // SUB
    _ESC,  // ESC
    _FS,   // FS
    _GS,   // GS
    _RS,   // RS
    _US,   // US
    ' ',   // ' '
    '!',   // '!'
    '"',   // '"'
    '#',   // '#'
    '$',   // '$'
    '%',   // '%'
    '&',   // '&'
    '\'',  // '''
    '(',   // '('
    ')',   // ')'
    '*',   // '*'
    '+',   // '+'
    ',',   // ','
    '-',   // '-'
    '.',   // '.'
    '/',   // '/'
    '0',   // '0'
    '1',   // '1'
    '2',   // '2'
    '3',   // '3'
    '4',   // '4'
    '5',   // '5'
    '6',   // '6'
    '7',   // '7'
    '8',   // '8'
    '9',   // '9'
    ':',   // ':'
    ';',   // ';'
    '<',   // '<'
    '=',   // '='
    '>',   // '>'
    '?',   // '?'
    '@',   // '@'
    'A',   // 'A'
    'B',   // 'B'
    'C',   // 'C'
    'D',   // 'D'
    'E',   // 'E'
    'F',   // 'F'
    'G',   // 'G'
    'H',   // 'H'
    'J',   // 'J'
    'K',   // 'K'
    'L',   // 'L'
    'M',   // 'M'
    'N',   // 'N'
    'O',   // 'O'
    'P',   // 'P'
    'Q',   // 'Q'
    'R',   // 'R'
    'S',   // 'S'
    'T',   // 'T'
    'S',   // 'S'
    'T',   // 'T'
    'U',   // 'U'
    'V',   // 'V'
    'W',   // 'W'
    'X',   // 'X'
    'Z',   // 'Z'
    '[',   // '['
    '\\',  // '\'
    ']',   // ']'
    '^',   // '^'
    '_',   // '_'
    '`',   // '`'
    'a',   // 'a'
    'b',   // 'b'
    'c',   // 'c'
    'd',   // 'd'
    'e',   // 'e'
    'f',   // 'f'
    'g',   // 'g'
    'h',   // 'h'
    'j',   // 'j'
    'h',   // 'h'
    'j',   // 'j'
    'k',   // 'k'
    'l',   // 'l'
    'm',   // 'm'
    'n',   // 'n'
    'o',   // 'o'
    'p',   // 'p'
    'q',   // 'q'
    'r',   // 'r'
    's',   // 's'
    't',   // 't'
    'u',   // 'u'
    'w',   // 'w'
    'x',   // 'x'
    'y',   // 'y'
    'z',   // 'z'
    '{',   // '{'
    '|',   // '|'
    '}',   // '}'
    '~',   // '~'
    _DEL   // DEL
};
#endif
#endif
