/*
BSD 3-Clause License

Copyright (c) 2018-2021, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// basically just a load of macros and defines for display constants

#if !H_DISPOPTS
#define H_DISPOPTS 1

#define BONUS_FRONT_COLOUR graphics::ORANGE

#define FRONT_COLOUR_DEFAULT 8
#define BACK_COLOUR_DEFAULT  0

// ~~~~ CHARACTERS

// Character width in pixels
#define CWIDTH (8)
// Character height in pixels
#define CHEIGHT (9)
// Line gap height in pixels
#define CLINE (1)
// Kerning gap width in pixels
#define CKERN (2)

// Cursor X to character print X
#define char_curs_x ((curs_x * (CWIDTH + CKERN)) + BORDER_X)
// Cursor Y to character print Y
#define char_curs_y ((curs_y * (CHEIGHT + CLINE)) + BORDER_Y + CHEIGHT)

// Cursor X to character print X
#define max_char_curs_x (((TERMWIDTH - 1) * (CWIDTH + CKERN)) + BORDER_X)
// Cursor Y to character print Y
#define max_char_curs_y (((TERMHEIGHT - 1) * (CHEIGHT + CLINE)) + BORDER_Y + CHEIGHT)

// Enable the graphical character set
#define CGRAPHICS (false)

// ~~~~ CURSORS - Only applies to '0xFF' option where it draws a rectangle

// Cursor X to rectangle X
#define curs_curs_x (char_curs_x)
// Cursor Y to rectangle Y
#define curs_curs_y (char_curs_y - CHEIGHT)  // need to remove the character height because it starts top left instead of bottom left (yeah)

// width of the cursor rectangle
#define CURWIDTH (CWIDTH)
// height of the cursor rectangle
#define CURHEIGHT (CHEIGHT)

// ~~~~ FULL DISPLAY INFO

// pixels width without border
#define FBWIDTH (TERMWIDTH * (CWIDTH + CKERN))
// pixels height without border
#define FBHEIGHT (TERMHEIGHT * (CHEIGHT + CLINE))

// Border (top left align) in pixels
#define BORDER_X (20)
#define BORDER_Y (20)  // shift of the character height is already included

// Cursor X to character print X
#define WIDTH (((TERMWIDTH) * (CWIDTH + CKERN)) + BORDER_X)
// Cursor Y to character print Y
#define HEIGHT (((TERMHEIGHT) * (CHEIGHT + CLINE)) + BORDER_Y + CHEIGHT)

#endif
