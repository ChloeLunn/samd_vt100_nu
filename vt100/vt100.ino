/*
BSD 3-Clause License

Copyright (c) 2018-2021, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <elapsedMillis.h>  // timer

#include "dispopts.h"      // display options
#include "graphdrive.hpp"  // graphics driver
#include "termopts.h"      // terminal options
#include "vt100_rom.h"     // rom files from the OG VT100 (used for font)

#define CTS A6  // Clear-To-Send (unused) assumes that the connected device always wants to send
#define RTS A5  // Request-TO-Send (LOW to receive, HIGH to request end to sending)

#define BELL_SPEAK 8  // Speaker pin for bells, etc.
#define BELL_LED   7  // LED pin for bells

#define THROTTLE_DELAY  // delayMicroseconds(1000);

unsigned short ansi_colour[] = {graphics::BACK_BLACK, graphics::RED, graphics::GREEN, graphics::YELLOW, graphics::BLUE, graphics::MAGENTA, graphics::CYAN, graphics::WHITE, BONUS_FRONT_COLOUR};
int front_colour = FRONT_COLOUR_DEFAULT;
int back_colour = BACK_COLOUR_DEFAULT;
int font_size = 1;

bool inverted_video = false;

// Buffer for the locations of tabstops
char tabstop[TERMWIDTH];
// Current character/scroll buffer (not that there's enough to scroll back...)
char dispBuf[TERMHEIGHT][TERMWIDTH];
// Graphical character flags (MUST be same size as dispBuf)
char graph_flags[TERMHEIGHT][TERMWIDTH];

// current cursor position
byte curs_x = 0;
byte curs_y = 0;
byte saved_curs[2];  // x = 0. y = 1

bool follow_vt52 = VT52_CMD;

// transmit status
bool xon = true;
volatile bool ext_xon = true;

// whether to print graphics and not characters
bool graphics_mode = false;

// Cursor flashing state
bool cursor_state = false;

// Used to time the cursor flash
elapsedMillis flash_throttle;

// String used for processing escaped sequences
char escape_sequence[15] = {[0] = 0};
int escape_len = 0;

// assert rts and xon/xoff
void assert_rts(bool on)
{
    if (on && !ext_xon)
    {
        digitalWrite(RTS, HIGH);
        TERMDEVICE.write(_DC3);  // XOFF
        ext_xon = on;
    }
    else if (!on && ext_xon)
    {
        digitalWrite(RTS, LOW);
        TERMDEVICE.write(_DC1);  // XON
        ext_xon = on;
    }
}

// Check if we need to assert RTS, and do it
void check_rts_assert()
{
    // if we reached the end of the buffer, assert the "shut up for a minute" line
    if (TERMDEVICE.available() >= RTS_ASSERT)
        assert_rts(true);
    // wait until it's cleared down to low numbers
    else if (TERMDEVICE.available() <= RTS_DEASSERT)
        assert_rts(false);
}

// clear and then reprint the screen
void reprint_screen()
{
    // Assert RTS to stop sending while we scroll
    assert_rts(true);

    graphics::clear_display();

    int t_x = curs_x;
    int t_y = curs_y;

    cursor_home();

    // foreach line
    for (curs_y = 0; curs_y < TERMHEIGHT; curs_y++)
    {
        // foreach character
        for (curs_x = 0; curs_x < TERMWIDTH; curs_x++)
        {
            set_cursor();  // set the cursor to that line

            // if there is a valid character
            if (isprint(dispBuf[curs_y][curs_x]))
            {
#if CGRAPHICS
                if (graph_flags[curs_y][curs_x])
                    tft_print_vt(dispBuf[curs_y][curs_x] - '@', ansi_colour[front_colour], ansi_colour[back_colour]);
                else
#endif
                    tft_print_vt(dispBuf[curs_y][curs_x], ansi_colour[front_colour], ansi_colour[back_colour]);
            }
            // assume non-print is end of line
            else
            {
                // stop and go to next line
                curs_x = 0;
                break;
            }
        }
    }

    // set cursor back to previous position
    curs_y = t_y;
    curs_x = t_x;
    set_cursor();
}

// Scroll the terminal lines (more == more efficient)
void scroll_term_by(int by)
{
    // if we're scrolling by the whole screen, then just do a clear as it's faster
    if (TERMHEIGHT == by)
    {
        // Assert RTS to stop sending while we scroll
        assert_rts(true);
        clear();
        return;
    }

// smooth display driver scroll
#if TERMSCROLL == 1

    // Assert RTS to stop sending while we scroll
    assert_rts(true);

    // scroll display up lines graphically
    graphics::scroll_lines(((CHEIGHT + CLINE) * by));

    // edit text buffer to reflect scrolling
    for (curs_y = 0; curs_y < TERMHEIGHT; curs_y++)
    {
        // foreach character
        for (curs_x = 0; curs_x < TERMWIDTH; curs_x++)
        {
            // copy the scrolled line into the top
            if (curs_y + by < TERMHEIGHT)
            {
                dispBuf[curs_y][curs_x] = dispBuf[curs_y + by][curs_x];
                graph_flags[curs_y][curs_x] = CGRAPHICS && graph_flags[curs_y + by][curs_x];

                // assume non-print is end of line
                if (!isPrintable(dispBuf[curs_y][curs_x]))
                {
                    // stop and go to next line
                    curs_x = 0;
                    break;
                }
            }
            // clear the lines at the end (possibly not needed)
            else
            {
                dispBuf[curs_y][curs_x] = 0;
                graph_flags[curs_y][curs_x] = 0;
            }
        }
    }

#else

    // Assert RTS to stop sending while we scroll
    assert_rts(true);

    // clear screen back to first character (DO NOT use clear()! It wipes the text buffer, too)
    graphics::clear_display();
    cursor_home();

    // foreach line
    for (curs_y = 0; curs_y < TERMHEIGHT; curs_y++)
    {
        // foreach character
        for (curs_x = 0; curs_x < TERMWIDTH; curs_x++)
        {
            set_cursor();  // set the cursor to that line

            // copy the scrolled line into the top
            if (curs_y + by < TERMHEIGHT)
            {
                dispBuf[curs_y][curs_x] = dispBuf[curs_y + by][curs_x];
                graph_flags[curs_y][curs_x] = CGRAPHICS && graph_flags[curs_y + by][curs_x];

                // if there is a valid character
                if (isPrintable(dispBuf[curs_y][curs_x]))
                {
                    if (CGRAPHICS && graph_flags[curs_y][curs_x])
                        tft_print_vt(dispBuf[curs_y][curs_x] - '@', ansi_colour[front_colour], ansi_colour[back_colour]);
                    else
                        tft_print_vt(dispBuf[curs_y][curs_x], ansi_colour[front_colour], ansi_colour[back_colour]);
                }
                // assume non-print is end of line
                else
                {
                    // stop and go to next line
                    curs_x = 0;
                    break;
                }
            }
            // clear the lines at the end (possibly not needed)
            else
            {
                dispBuf[curs_y][curs_x] = 0;
                graph_flags[curs_y][curs_x] = 0;
            }
        }
    }
#endif

    // set cursor to new position
    curs_y = TERMHEIGHT - by;
    curs_x = 0;
    set_cursor();
}

// Scroll the terminal by the fixeddefined value
void scroll_term()
{
    scroll_term_by(TERMSCROLL);
}

// Move the cursor to a new line. If b then move X to 0, optionally scroll when at end of terminal
void new_line(bool b, bool no_scroll)
{
    if (b)
        curs_x = 0;  // go to beginning of line
    curs_y++;        // increment line counter

    if (curs_y >= TERMHEIGHT)  // if we've hit the end of the terminal
    {
        curs_y = 0;  // reset to line 1
        if (!no_scroll)
            scroll_term();  // scroll the screen
    }
    set_cursor();  // set the cursor
}

// Move the cursor to a new line. If b then move X to 0
void new_line(bool b)
{
    new_line(b, false);
}

// reset the screen/font settings
void reset()
{
    font_size = 1;
    front_colour = FRONT_COLOUR_DEFAULT;
    back_colour = BACK_COLOUR_DEFAULT;
    graphics_mode = false;
    inverted_video = false;
}

// reset the screen/font settings
void clear_reset_all()
{
    xon = true;
    curs_x = 0;
    curs_y = 0;
    saved_curs[0] = 0;
    saved_curs[1] = 0;
    follow_vt52 = VT52_CMD;

    reset();
    clear();

    // reset tabstops
    for (curs_x = 0; curs_x < TERMWIDTH; curs_x++)
        tabstop[curs_x] = 0;  // clear
    for (curs_x = 0; curs_x <= TERMWIDTH - 1; curs_x += TABWIDTH)
        tabstop[curs_x] = 0xFF;  // set to default

    tabstop[TERMWIDTH - 1] = 1;  // last column is a tabstop

#if AUDIOB_BELL
    pinMode(BELL_SPEAK, OUTPUT);
    noTone(BELL_SPEAK);
#endif

#if LED_BELL
    pinMode(BELL_LED, OUTPUT);
    digitalWrite(BELL_LED, LOW);
#endif

    cursor_home();
    do_bell();
}

// Clear the screen
void clear()
{
    assert_rts(true);
    for (int y = 0; y < TERMHEIGHT; y++)
        for (int x = 0; x < TERMWIDTH; x++)
        {
            graph_flags[y][x] = 0;
            dispBuf[y][x] = 0;
        }
    graphics::clear_display();
    cursor_home();
}

// Clear to end of line, leaves cursor where it is
void clear_to_end_line()
{
    assert_rts(true);

    set_cursor();

    // clear display data
    for (int x = curs_x; x < TERMWIDTH; x++)
    {
        graph_flags[curs_y][x] = 0;
        dispBuf[curs_y][x] = 0;
    }

    // Graphically clear rest of line
    graphics::clear_section(char_curs_x, char_curs_y - CHEIGHT, WIDTH, char_curs_y);

    set_cursor();
}

// Clear to end of screen, leaves cursor where it is
void clear_to_end_screen()
{
    assert_rts(true);

    // clear end of line
    clear_to_end_line();

    // clear display data after this line
    for (int y = curs_y + 1; y < TERMHEIGHT; y++)
    {
        for (int x = 0; x < TERMWIDTH; x++)
        {
            graph_flags[y][x] = 0;
            dispBuf[y][x] = 0;
        }
    }

    // Graphically clear rest of screen
    graphics::clear_section(0, char_curs_y, WIDTH, HEIGHT);

    set_cursor();
}

// Clear to end of line, leaves cursor where it is
void clear_to_start_line()
{
    assert_rts(true);

    set_cursor();

    // clear display data
    for (int x = 0; x <= curs_x; x++)
    {
        graph_flags[curs_y][x] = 0;
        dispBuf[curs_y][x] = 0;
    }

    // Graphically clear rest of line
    graphics::clear_section(0, char_curs_y - CHEIGHT, char_curs_x, char_curs_y);

    set_cursor();
}

// Clear to end of screen, leaves cursor where it is
void clear_to_start_screen()
{
    assert_rts(true);

    // clear in this line
    clear_to_start_line();

    // clear display data on previous lines
    for (int y = 0; y <= curs_y; y++)
    {
        for (int x = 0; x < TERMWIDTH; x++)
        {
            graph_flags[y][x] = 0;
            dispBuf[y][x] = 0;
        }
    }

    // Graphically clear start of screen
    graphics::clear_section(0, 0, WIDTH, char_curs_y - CHEIGHT - CLINE);
}

// Set the cursor to the current x,y position
void set_cursor()
{
    // if out of bounds, set it to be inside bounds
    if (curs_x >= TERMWIDTH)
        curs_x = TERMWIDTH - 1;
    if (curs_y >= TERMHEIGHT)
        curs_y = TERMHEIGHT - 1;

    graphics::set_cursor(char_curs_x, char_curs_y);
}

// Move the cursor to home
void cursor_home()
{
    curs_x = 0;
    curs_y = 0;
    set_cursor();
}

// Flash the cursor on
void cursor_on()
{
#if CURCHAR == _NUL
    // don't do anything here, cursor is disabled
#elif CURCHAR != 0xFF
    tft_print_vt(CURCHAR, ansi_colour[front_colour], ansi_colour[back_colour]);
#else
    graphics::draw_rectangle(curs_curs_x, curs_curs_y, CURWIDTH, CURHEIGHT, ansi_colour[front_colour]);
#endif
}

// Flash the cursor off
void cursor_off()
{
#if CURCHAR == _NUL
    // don't do anything here, cursor is disabled
#elif CURCHAR != 0xFF
    tft_print_vt(CURCHAR, ansi_colour[back_colour], ansi_colour[back_colour]);
#else
    graphics::draw_rectangle(curs_curs_x, curs_curs_y, CURWIDTH, CURHEIGHT, ansi_colour[back_colour]);
#endif

    // if there is a character in the buffer, write it out because we need to show it
    if (dispBuf[curs_y][curs_x] != 0)
    {
        set_cursor();
        if (CGRAPHICS && graph_flags[curs_y][curs_x])
            tft_print_vt(dispBuf[curs_y][curs_x] - '@', ansi_colour[front_colour], ansi_colour[back_colour]);
        else
            tft_print_vt(dispBuf[curs_y][curs_x], ansi_colour[front_colour], ansi_colour[back_colour]);
    }
}

// Flash the cursor
void flash_cursor()
{
    set_cursor();  // make sure the tft cursor matches our buffer cursor

    cursor_state = !cursor_state;

    if (cursor_state)
        cursor_on();
    else
        cursor_off();

    set_cursor();  // make sure the tft cursor matches our buffer cursor
}

// Move the cursor left by num. stop if hit left edge
void cursor_left(int num)
{
    // TERMDEVICE.printf("Moving left by %i\r\n", num);
    while (num > 0)
    {
        if (curs_x > 0)
            curs_x -= 1;
        else
            break;
        num--;
    }
    set_cursor();
}

// Move the cursor right by num. stop if hit right edge
void cursor_right(int num)
{
    // TERMDEVICE.printf("Moving right by %i\r\n", num);
    while (num > 0)
    {
        if (curs_x < TERMWIDTH - 1)
            curs_x += 1;
        else
            break;
        num--;
    }
    set_cursor();
}

// Move the cursor up by num. stop if hit top edge
void cursor_up(int num)
{
    // TERMDEVICE.printf("Moving up by %i\r\n", num);
    while (num > 0)
    {
        if (curs_y > 0)
            curs_y -= 1;
        else
            break;
        num--;
    }
    set_cursor();
}

// Move the cursor down by num. stop if hit bottom edge
void cursor_down(int num)
{
    // TERMDEVICE.printf("Moving down by %i\r\n", num);
    while (num > 0)
    {
        if (curs_y < TERMHEIGHT - 1)
            curs_y += 1;
        else
            break;
        num--;
    }
    set_cursor();
}

// process and ansi CSI command (e.g \033[31m to make text red; ONLY UNCLUDE NUMBER AND END LETTER)
void do_csi(const char* str, int len)
{
    char* cmd = (char*)str;

    bool isDEC = cmd[0] == '?';
    if (isDEC)
        cmd++;

        /* Special DEC controls
         *  ESC [ ? 1 5 n     request printer status
         *  ESC [ ? 1 0 n     report - printer ready
         *  ESC [ ? 6 h       turn on region - origin mode
         *  ESC [ ? 6 l       turn off region - full screen mode
         *  ESC [ ? 1 h       cursor keys in applications mode
         *  ESC [ ? 1 l       cursor keys in cursor positioning mode
         *  ESC [ ? 5 h       black characters on white screen mode
         *  ESC [ ? 5 l       white characters on black screen mode
         *  ESC [ ? 7 h       auto wrap to new line
         *  ESC [ ? 7 l       auto wrap off
         *  ESC [ ? 8 h       keyboard auto repeat mode on
         *  ESC [ ? 8 l       keyboard auto repeat mode off
         *  ESC [ ? 9 h       480 scan line mode
         *  ESC [ ? 9 l       240 scan line mode
         *  ESC [ ? 1 8 h     print form feed on
         *  ESC [ ? 1 8 l     print form feed off
         *  ESC [ ? 1 9 h     print whole screen
         *  ESC [ ? 1 9 l     print only scroll region
         *  ESC [ 2 0 h       newline mode LF, FF, VT, CR = CR/LF)  -- no '?' ???
         *  ESC [ 2 0 l       line feed mode (LF, FF, VT = LF ; CR = CR) -- no '?' ???
         */

#define a_val (params[0])
#define b_val (params[1])
#define c_val (params[2])

    int params[5] = {0, 0, 0, 0, 0};

    // load possible params
    for (int idx = 0; idx < 5; idx++)
    {
        // load the value
        params[idx] = atoi(cmd);

        // scrobble cmd to next ';'
        for (; cmd; cmd++)
        {
            // if ';' then add one and continue to next atoi
            if (cmd[0] == ';')
            {
                cmd++;
                break;
            }

            // if end, then break both loops
            if (cmd[0] == 0 || isalpha(cmd[0]))
            {
                idx = 6;
                break;
            }
        }
    }

    char c = str[len - 1];  // str NOT cmd because that way we can use len without checking isDEC

    // Serial.printf("\r\nAnsi code: \\033[%i;%i;%i;%i;%i%c\r\n", params[0], params[1], params[2], params[3], params[4], c);

    switch (c)
    {
        // UPPERCASE CONTROLS

    case 'A':  // cursor up
        if (a_val == 0)
            a_val = 1;
        cursor_up(a_val);
        return;
    case 'B':  // cursor down
        if (a_val == 0)
            a_val = 1;
        cursor_down(a_val);
        return;
    case 'C':  // cursor right
        if (a_val == 0)
            a_val = 1;
        cursor_right(a_val);
        return;
    case 'D':  // cursor left
        if (a_val == 0)
            a_val = 1;
        cursor_left(a_val);
        return;
    case 'E':  // move cursor to beginning of next line
        if (a_val == 0)
            a_val = 1;
        cursor_down(a_val);
        curs_x = 0;
        set_cursor();
        return;
    case 'F':  // move cursor to beginning of previous line
        if (a_val == 0)
            a_val = 1;
        cursor_up(a_val);
        curs_x = 0;
        set_cursor();
        return;
    case 'G':  // move cursor to column
        if (a_val == 0)
            a_val = 1;
        curs_x = a_val - 1;
        set_cursor();
        return;
    case 'H':  // cursor position
        if (a_val == 0)
            a_val = 1;
        if (b_val == 0)
            b_val = 1;
        if (b_val >= 1 && a_val >= 1)
        {
            curs_x = b_val - 1;
            curs_y = a_val - 1;
        }
        set_cursor();
        return;
    case 'J':  // Erase in display - cursor does not change
        {
            if (a_val == 0)  // clear to end of screen
                clear_to_end_screen();
            else if (a_val == 1)  // clear from beginning of screen
                clear_to_start_screen();
            else if (a_val == 2)  // clear all (move cursor top left)
                clear();
            else if (a_val == 3)  // clear all and delete scroll back (which we don't have anyway)
                clear();

            set_cursor();
        }
        return;
    case 'K':  // Erase in line - cursor does not change
        {
            if (a_val == 0)  // clear to end of line
                clear_to_end_line();
            else if (a_val == 1)  // clear from beginning of line
                clear_to_start_line();
            else if (a_val == 2)  // clear all (move cursor to left)
            {
                curs_x = 0;
                clear_to_end_line();
            }

            set_cursor();
        }
        return;
    case 'S':  // Scroll term up by n lines
        if (a_val == 0)
            a_val = 1;
        scroll_term_by(a_val);
        return;
    case 'T':  // Scroll term down by n (not implemented)
        return;

        // LOWERCASE CONTROLS

    case 'c':  // request terminal model identifier
        TERMDEVICE.print(TERMIDENT_ANSI);
        return;
    case 'f':  // cursor position (alt)
        if (a_val == 0)
            a_val = 1;
        if (b_val == 0)
            b_val = 1;
        if (b_val >= 1 && a_val >= 1)
        {
            curs_x = b_val - 1;
            curs_y = a_val - 1;
            set_cursor();
        }
        return;
    case 'h':
        {
            if (a_val == 0)
                a_val = 1;
            if (isDEC)
            {
                switch (a_val)
                {
                case 2:  // switch from VT52 mode
                    follow_vt52 = false;
                    break;
                case 4:  // smooth scroll
                case 3:  // switch to 132 columns
                default:
                    break;
                }
            }
        }
        return;
    case 'i':  // change aux port status
        return;
    case 'l':
        {
            if (a_val == 0)
                a_val = 1;
            if (isDEC)
            {
                switch (a_val)
                {
                case 2:  // switch to VT52 mode
                    follow_vt52 = true;
                    break;
                case 4:  // jump scroll
                case 3:  // switch to 80 columns
                default:
                    break;
                }
            }
        }
        return;
    case 'm':  // Change style of characters
        {
            for (int i = 0; i < 5; i++)
            {
                switch (params[i])
                {
                case 0:
                    reset();
                    break;
                case 3:
                    inverted_video = true;
                    break;
                case 23:
                    inverted_video = false;
                    break;
                case 7:
                    inverted_video = true;
                    break;
                case 27:
                    inverted_video = false;
                    break;

                case 30:
                    front_colour = 0;  // black
                    break;
                case 31:
                    front_colour = 1;  // red
                    break;
                case 32:
                    front_colour = 2;  // green
                    break;
                case 33:
                    front_colour = 3;  // yellow
                    break;
                case 34:
                    front_colour = 4;  // blue
                    break;
                case 35:
                    front_colour = 5;  // magenta
                    break;
                case 36:
                    front_colour = 6;  // cyan
                    break;
                case 37:
                    front_colour = 7;  // white
                    break;

                case 40:
                    front_colour = 0;  // black
                    break;
                case 41:
                    front_colour = 1;  // red
                    break;
                case 42:
                    front_colour = 2;  // green
                    break;
                case 43:
                    front_colour = 3;  // yellow
                    break;
                case 44:
                    front_colour = 4;  // blue
                    break;
                case 45:
                    front_colour = 5;  // magenta
                    break;
                case 46:
                    front_colour = 6;  // cyan
                    break;
                case 47:
                    front_colour = 7;  // white
                    break;

                case 90:
                    front_colour = 0;  // black
                    break;
                case 91:
                    front_colour = 1;  // red
                    break;
                case 92:
                    front_colour = 2;  // green
                    break;
                case 93:
                    front_colour = 3;  // yellow
                    break;
                case 94:
                    front_colour = 4;  // blue
                    break;
                case 95:
                    front_colour = 5;  // magenta
                    break;
                case 96:
                    front_colour = 6;  // cyan
                    break;
                case 97:
                    front_colour = 7;  // white
                    break;

                case 100:
                    front_colour = 0;  // black
                    break;
                case 101:
                    front_colour = 1;  // red
                    break;
                case 102:
                    front_colour = 2;  // green
                    break;
                case 103:
                    front_colour = 3;  // yellow
                    break;
                case 104:
                    front_colour = 4;  // blue
                    break;
                case 105:
                    front_colour = 5;  // magenta
                    break;
                case 106:
                    front_colour = 6;  // cyan
                    break;
                case 107:
                    front_colour = 7;  // white
                    break;

                default:
                    break;
                }
            }
        }
        return;
    case 'n':  // device status report
        if (a_val == 0)
            a_val = 1;
        if (a_val == 6)
        {
            // report cursor position
            TERMDEVICE.print("\033[");
            TERMDEVICE.print(curs_y + 1);
            TERMDEVICE.print(';');
            TERMDEVICE.print(curs_x + 1);
            TERMDEVICE.print('R');
        }
        return;
    case 'q':
        {
            // do LEDs here
        }
        return;
    case 'r':  // set margins (ignore this)
        return;
    case 's':  // save cursor position
        saved_curs[0] = curs_x;
        saved_curs[1] = curs_y;
        return;
    case 'u':  // restore cursor position
        curs_x = saved_curs[0];
        curs_y = saved_curs[1];
        set_cursor();
        return;
    default:
        break;
    }

    return;
}

// process an ansi command
bool do_ansi(const char* str, int len)
{
#if ANSI_CMD
    // ANSI
    switch (str[0])
    {
    case 'E':  // New line
        new_line(true);
        return true;
    case 'D':  // New Line, no return to 0
        new_line(false);
        return true;
    case 'M':  // Reverse Index (move up a line, scroll backwards if at top)
        cursor_up(1);
        return true;
    case 'c':  // Reset to initial state and clear
        clear_reset_all();
        return true;
    case 'H':  // Set a tabstop at the current column
        tabstop[curs_x] = 0xFF;
        return true;
    case '[':  // CSIs (very like VT52s)
        do_csi(&str[1], len - 1);
        return true;
    case 'Z':                              // What Are You (same as <ESC>[c and <ESC>[0c), <ESC>Z is not recommended as VT52 overlap
        TERMDEVICE.print(TERMIDENT_ANSI);  // ID as base model VT100
        return true;
    default:  // If we don't know it, then leave
        break;
    }
#endif
    return false;
}

// Process a vt52 command
bool do_vt52(const char* str, int len)
{
#if VT52_CMD
    if (follow_vt52)
    {
        char c = str[0];

        switch (c)
        {
        case 'A':  // Cursor Up
            cursor_up(1);
            return true;
        case 'B':  // Cursor Down
            cursor_down(1);
            return true;
        case 'C':  // Cursor Right
            cursor_right(1);
            return true;
        case 'D':  // Cursor Left
            cursor_left(1);
            return true;
        case 'E':  // Clear screen and place cursor top left (we cheat by doing actual clear)
            clear();
            return true;
        case 'F':  // Enter Graphics Mode
            graphics_mode = true;
            return true;
        case 'G':  // Exit Graphics Mode
            graphics_mode = false;
            return true;
        case 'H':  // Move Cursor to home
            cursor_home();
            return true;
        case 'I':          // Reverse Line Feed (insert line above, then go to it)
            cursor_up(1);  // non-standard, no backwards scroll and not insert
            return true;
        case 'J':  // Erase to end of screen from current cursor, do not move cursor
            clear_to_end_screen();
            return true;
        case 'K':  // Erase all characters from cursor to end of line, do not move cursor
            clear_to_end_line();
            return true;
        case 'L':  // Insert a line (treat as line feed)
            new_line(true);
            return true;
        case 'M':  // Remove a line (treat as delete current line and then carriage return)
            curs_x = 0;
            clear_to_end_line();
            return true;
        case 'Y':  // Move cursor to a specified position "\033Y[r][c]"
            {
r_again:
                // read in the next values for use as cursors wait a bit, just to be super sure they're in the buffer
                THROTTLE_DELAY
                int r = TERMDEVICE.read() - 31;
                if (r < 0)
                    goto r_again;
c_again:
                THROTTLE_DELAY
                int c = TERMDEVICE.read() - 31;
                if (c < 0)
                    goto c_again;

                // if valid, set cursor
                if (c >= 1 && r >= 1)
                {
                    curs_x = c - 1;
                    curs_y = r - 1;
                    set_cursor();
                }
            }
            return true;
        case 'Z':
            TERMDEVICE.print(TERMIDENT_VT52);
            return true;
        case '=':  // Alternate keypad mode - trapped but does nothing
            return true;
        case '>':  // Exit alternate keypad mode - trapped but does nothing
            return true;
        case '<':  // Enter ANSI mode, ignore VT52 codes
            follow_vt52 = false;
            return true;
        default:
            break;
        }
    }
#endif
    return false;
}

bool isAnsi(char c)
{
#if ANSI_CMD
    return c == '\\';  // terminating 'string terminator'
#endif
    return false;
}

// is the character part of a VT52 escape sequence?
bool isvt52(char c)
{
#if VT52_CMD
    if (follow_vt52)
        return c == '#' || c == '=' || c == '>' || c == '<' || isalpha(c);
#endif
    return false;
}

// process reading and processing ANSI/VT52 escape commands
void do_escaped()
{
    escape_sequence[0] = 0;
    escape_len = 0;
    char c = 0;

    // Read in the command
    while (TERMDEVICE.available() > 0)
    {
        // ascii 7-bit characters only!
        c = TERMDEVICE.read();

        if (c <= 0)
            continue;

        // if another escape is read, then quit without processing
        if (c == _ESC || escape_len >= 14)
            return;

        // else, add it to the string
        strncat(escape_sequence, &c, 1);
        escape_len++;

        /* allowed terminating characters */
        if (isalpha(c) || isvt52(c) || isAnsi(c))
        {
            break;
        }
    }

#if ANSI_CMD
    // if we're allowing ANSI (or VT) commands and we have a command with a valid length
    if (escape_len >= 1)
    {
        // Check the first letter first
        if (do_vt52(escape_sequence, escape_len))
        {
            return;
        }

        if (do_ansi(escape_sequence, escape_len))
        {
            return;
        }
    }
#endif

    return;
}

// Do a Carriage return (checks newline behaviour)
void do_cr()
{
#if _NL == _CR
    new_line(true);
#else
    curs_x = 0;
    set_cursor();
#endif
}

// Do a horizontal tab
void do_tab()
{
    // start at current cursor +1 (so that if on a tab, we skip it).
    // then go down the line until we hit either the tabstop or end of line
    for (curs_x = curs_x + 1; curs_x < TERMWIDTH; curs_x++)
    {
        if (tabstop[curs_x])
            break;
    }

    set_cursor();
}

// puts the character on the screen using the OG vt100 font rom file
void tft_print_vt(char c, int colour, int backcolour)
{
    // if (inverted_video)  // flip colour and backcolour
    // {
    //     char newbmp[CHEIGHT];
    //     for (int i = 0; i < CHEIGHT; i++)
    //     {
    //         newbmp[i] = ~font_bitmaps[c][i];
    //     }

    //     // fill space between chars with rectangle so that it's not blocks, but smooth invertion
    //     graphics::draw_bitmap(char_curs_x - CKERN, char_curs_y - CHEIGHT, font_bitmaps[0x82], CKERN, CHEIGHT, colour, backcolour);
    //     // write out inverted character
    //     graphics::draw_bitmap(char_curs_x, char_curs_y - CHEIGHT, (const unsigned char*)newbmp, CWIDTH, CHEIGHT, colour, backcolour);
    // }
    // else
    graphics::draw_bitmap(char_curs_x, char_curs_y - CHEIGHT, font_bitmaps[c], CWIDTH, CHEIGHT, colour, backcolour);
}

// Print a character
bool print_char(char c, bool no_scroll)
{
    if (!isprint(c))
        return 0;

    set_cursor();

    // If the character is not blank, then we need to clear the old character
    if (dispBuf[curs_y][curs_x] != 0)
    {
#if CGRAPHICS
        if (graph_flags[curs_y][curs_x])
            tft_print_vt(dispBuf[curs_y][curs_x] - '@', ansi_colour[back_colour], ansi_colour[back_colour]);
        else
#endif
            tft_print_vt(dispBuf[curs_y][curs_x], ansi_colour[back_colour], ansi_colour[back_colour]);
        set_cursor();
    }

    if (c != ' ')
    {
#if UCASE
        dispBuf[curs_y][curs_x] = toupper(c);
#else
        dispBuf[curs_y][curs_x] = c;
#endif

#if CGRAPHICS
        if (graphics_mode)
            dispBuf[curs_y][curs_x] = toupper(c);
        graph_flags[curs_x][curs_x] = graphics_mode;
#endif

#if CGRAPHICS
        if (graphics_mode)
            tft_print_vt(dispBuf[curs_y][curs_x] - '@', ansi_colour[front_colour], ansi_colour[back_colour]);
        else
#endif
            tft_print_vt(dispBuf[curs_y][curs_x], ansi_colour[front_colour], ansi_colour[back_colour]);
    }
    else
    {
        dispBuf[curs_y][curs_x] = 0;
    }

    curs_x++;  // increment cursor along one

    // if we've hit the width, do a new line
    if (curs_x >= TERMWIDTH)
    {
        new_line(true, no_scroll);
    }
    return true;
}

// Print a character
static inline bool print_char(char c)
{
    return print_char(c, false);
}

// Do the terminal bell
void do_bell()
{
#if LED_BELL || AUDIO_BELL || SCREEN_BELL
    assert_rts(true);
#endif

#if SCREEN_BELL
    graphics::fill_display(ansi_colour[front_colour]);
#endif

#if AUDIO_BELL
    tone(BELL_SPEAK, BELL_TONE, BELL_TIME);
#endif

#if (LED_BELL)
    digitalWrite(BELL_LED, HIGH);
#endif

#if LED_BELL || AUDIO_BELL || SCREEN_BELL
    delay(BELL_TIME);
#endif

#if (AUDIO_BELL)
    noTone(BELL_SPEAK);
#endif

#if (LED_BELL)
    digitalWrite(BELL_LED, LOW);
#endif

#if (SCREEN_BELL)
    reprint_screen();
#endif
}

// Setup and clear
void setup()
{
    graphics::initialise();

    // init the serial device
    TERMDEVICE.begin(TERMBAUD);

    // set up hardware RTS
    pinMode(RTS, OUTPUT);

    // set up hardware cts
    // pinMode(CTS, INPUT_PULLUP);  // not used

    // clear and reset
    clear_reset_all();

    // disable transmit until we enter loop
    ext_xon = false;
    assert_rts(true);
}

// Scan arriving data to display
void loop()
{
    // time to flash the cursor
    if (flash_throttle >= (CURBLINK / 2))
    {
        flash_cursor();
        flash_throttle = 0;
    }

    check_rts_assert();

    // while data is in the buffer, keep loading it without showing the cursor
    while (TERMDEVICE.available() > 0)
    {
        check_rts_assert();

        // disable cursor for now
        if (cursor_state)
        {
            flash_cursor();
        }

        // check_rts_assert();
        int c = TERMDEVICE.read();

        if (c <= 0)
            return;

            // If enabled, convert a DEL into BS (usually ignored)
#if DEL_DOES_BS
        if (c == _DEL)
            c = _BS;
#endif

            // remap the character if enabled
#if REMAP_WITH_TABLE
        c = ascii_chart[c];
#endif

        // try to print it
        if (!print_char((char)c))
        {
            // we can't, so handle it as a special character
            switch (c)
            {
            case _DC3:  // XOFF
                xon = false;
                break;
            case _DC1:  // XON
                xon = true;
                break;

            case _CR:  // Carriage return (CTRL+M), move cursor to start of line (or new_line)
                do_cr();
                break;

            case _BEL:  // Bell
                do_bell();
                break;

            // On a VT100 VT and FF inherit from LF:
            case _VT:  // Vertical Tab (CTRL+K), move cursor down a line and to beginning
            case _FF:  // Form feed (CTRL+L), move cursor down a line and to beginning
            case _LF:  // Line feed (CTRL+J), move cursor down a line and to beginning
                new_line(true);
                break;

            case _HT:  // Horizontal Tab (CTRL+I), Move the cursor to the next tab stop, or to the right margin if no further tab stops are present on the line
                do_tab();
                break;

            case _BS:  // Backspace (CTRL+H), Move the cursor to the left one character position, unless it is at the left margin, in which case no action
                cursor_left(1);
                break;

            case _DEL:  // Delete, Ignored on input (not stored in input buffer)
            case _NUL:  // NULL (CTRL+@), Ignored on input (not stored in input buffer)
                break;

            case _ESC:  // Escape (CTRL+[), ANSI control sequence (e.g. <ESC>[3~ for DEL)
                do_escaped();
                break;

            default:  // Any other character, do nothing
                break;
            }
        }
    }
    // sync the graphics up (e.g. composite or VGA)
    graphics::sync();
}
