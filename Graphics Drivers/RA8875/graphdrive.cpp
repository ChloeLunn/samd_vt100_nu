/*
BSD 3-Clause License

Copyright (c) 2018-2021, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "graphdrive.hpp"
#include <SPI.h>  // Only need SPI
#include <stdint.h>

#define SCANLINES (true)   // odd display lines are background coloured
#define LONGDOTS  (true)   // enable writing a full dot when going high->low on a left->right transition
#define HALFDOTS  (false)  // enable writing a half/mid dot when going high->low on a left->right transition
#define PREDOTS   (false)  // enable writing first dot on aleft->rigth;low->high transition as a half colour
#define MORELINES (false)  // disable double-height stretching to fit twice as many character rows on the screen

#if MORELINES
#undef SCANLINES
#define SCANLINES (false)
#endif

// on SAMD21 boards we want to create an SPI port on the same pins as we would use for an UNO/Nano
#if defined(__SAMD21G18A__)
#include "wiring_private.h"
SPIClass act_SPI(&sercom1, 12, 13, 11, SPI_PAD_0_SCK_1, SERCOM_RX_PAD_3);
#else
#define act_SPI SPI  // or you can change this if you want...
#endif

#define SPISPEED 8000000

namespace graphics {

// Driver constants
enum
{
    // Display Size - everything is hardcoded to these, so you'll need to change it if you want others
    RA8875_WIDTH = 800,
    RA8875_HEIGHT = 480,
    // Pin control info for different types of data
    RA8875_DATAWRITE = 0x00,
    RA8875_DATAREAD = 0x40,
    RA8875_CMDWRITE = 0x80,
    RA8875_CMDREAD = 0xC0,
    // Registers and values
    RA8875_PWRR = 0x01,
    RA8875_PWRR_DISPON = 0x80,
    RA8875_PWRR_DISPOFF = 0x00,
    RA8875_PWRR_SLEEP = 0x02,
    RA8875_PWRR_NORMAL = 0x00,
    RA8875_PWRR_SOFTRESET = 0x01,
    RA8875_MRWC = 0x02,
    RA8875_GPIOX = 0xC7,
    RA8875_PLLC1 = 0x88,
    RA8875_PLLC1_PLLDIV2 = 0x80,
    RA8875_PLLC1_PLLDIV1 = 0x00,
    RA8875_PLLC2 = 0x89,
    RA8875_PLLC2_DIV1 = 0x00,
    RA8875_PLLC2_DIV2 = 0x01,
    RA8875_PLLC2_DIV4 = 0x02,
    RA8875_PLLC2_DIV8 = 0x03,
    RA8875_PLLC2_DIV16 = 0x04,
    RA8875_PLLC2_DIV32 = 0x05,
    RA8875_PLLC2_DIV64 = 0x06,
    RA8875_PLLC2_DIV128 = 0x07,
    RA8875_SYSR = 0x10,
    RA8875_SYSR_8BPP = 0x00,
    RA8875_SYSR_16BPP = 0x0C,
    RA8875_SYSR_MCU8 = 0x00,
    RA8875_SYSR_MCU16 = 0x03,
    RA8875_PCSR = 0x04,
    RA8875_PCSR_PDATR = 0x00,
    RA8875_PCSR_PDATL = 0x80,
    RA8875_PCSR_CLK = 0x00,
    RA8875_PCSR_2CLK = 0x01,
    RA8875_PCSR_4CLK = 0x02,
    RA8875_PCSR_8CLK = 0x03,
    RA8875_HDWR = 0x14,
    RA8875_HNDFTR = 0x15,
    RA8875_HNDFTR_DE_HIGH = 0x00,
    RA8875_HNDFTR_DE_LOW = 0x80,
    RA8875_HNDR = 0x16,
    RA8875_HSTR = 0x17,
    RA8875_HPWR = 0x18,
    RA8875_HPWR_LOW = 0x00,
    RA8875_HPWR_HIGH = 0x80,
    RA8875_VDHR0 = 0x19,
    RA8875_VDHR1 = 0x1A,
    RA8875_VNDR0 = 0x1B,
    RA8875_VNDR1 = 0x1C,
    RA8875_VSTR0 = 0x1D,
    RA8875_VSTR1 = 0x1E,
    RA8875_VPWR = 0x1F,
    RA8875_VPWR_LOW = 0x00,
    RA8875_VPWR_HIGH = 0x80,
    RA8875_HSAW0 = 0x30,
    RA8875_HSAW1 = 0x31,
    RA8875_VSAW0 = 0x32,
    RA8875_VSAW1 = 0x33,
    RA8875_HEAW0 = 0x34,
    RA8875_HEAW1 = 0x35,
    RA8875_VEAW0 = 0x36,
    RA8875_VEAW1 = 0x37,
    RA8875_MCLR = 0x8E,
    RA8875_MCLR_START = 0x80,
    RA8875_MCLR_STOP = 0x00,
    RA8875_MCLR_READSTATUS = 0x80,
    RA8875_MCLR_FULL = 0x00,
    RA8875_MCLR_ACTIVE = 0x40,
    RA8875_DCR = 0x90,
    RA8875_DCR_LINESQUTRI_START = 0x80,
    RA8875_DCR_LINESQUTRI_STOP = 0x00,
    RA8875_DCR_LINESQUTRI_STATUS = 0x80,
    RA8875_DCR_CIRCLE_START = 0x40,
    RA8875_DCR_CIRCLE_STATUS = 0x40,
    RA8875_DCR_CIRCLE_STOP = 0x00,
    RA8875_DCR_FILL = 0x20,
    RA8875_DCR_NOFILL = 0x00,
    RA8875_DCR_DRAWLINE = 0x00,
    RA8875_DCR_DRAWTRIANGLE = 0x01,
    RA8875_DCR_DRAWSQUARE = 0x10,
    RA8875_ELLIPSE = 0xA0,
    RA8875_ELLIPSE_STATUS = 0x80,
    RA8875_MWCR0 = 0x40,
    RA8875_MWCR0_GFXMODE = 0x00,
    RA8875_MWCR0_TXTMODE = 0x80,
    RA8875_MWCR0_CURSOR = 0x40,
    RA8875_MWCR0_BLINK = 0x20,
    RA8875_MWCR0_DIRMASK = 0x0C,  ///< Bitmask for Write Direction
    RA8875_MWCR0_LRTD = 0x00,     ///< Left->Right then Top->Down
    RA8875_MWCR0_RLTD = 0x04,     ///< Right->Left then Top->Down
    RA8875_MWCR0_TDLR = 0x08,     ///< Top->Down then Left->Right
    RA8875_MWCR0_DTLR = 0x0C,     ///< Down->Top then Left->Right
    RA8875_BTCR = 0x44,
    RA8875_CURH0 = 0x46,
    RA8875_CURH1 = 0x47,
    RA8875_CURV0 = 0x48,
    RA8875_CURV1 = 0x49,
    RA8875_BECR0 = 0x50,
    RA8875_BECR1 = 0x51,
    RA8875_BTE_OW = 0xC0,
    RA8875_P1CR = 0x8A,
    RA8875_P1CR_ENABLE = 0x80,
    RA8875_P1CR_DISABLE = 0x00,
    RA8875_P1CR_CLKOUT = 0x10,
    RA8875_P1CR_PWMOUT = 0x00,
    RA8875_P1DCR = 0x8B,
    RA8875_P2CR = 0x8C,
    RA8875_P2CR_ENABLE = 0x80,
    RA8875_P2CR_DISABLE = 0x00,
    RA8875_P2CR_CLKOUT = 0x10,
    RA8875_P2CR_PWMOUT = 0x00,
    RA8875_P2DCR = 0x8D,
    RA8875_PWM_CLK_DIV1 = 0x00,
    RA8875_PWM_CLK_DIV2 = 0x01,
    RA8875_PWM_CLK_DIV4 = 0x02,
    RA8875_PWM_CLK_DIV8 = 0x03,
    RA8875_PWM_CLK_DIV16 = 0x04,
    RA8875_PWM_CLK_DIV32 = 0x05,
    RA8875_PWM_CLK_DIV64 = 0x06,
    RA8875_PWM_CLK_DIV128 = 0x07,
    RA8875_PWM_CLK_DIV256 = 0x08,
    RA8875_PWM_CLK_DIV512 = 0x09,
    RA8875_PWM_CLK_DIV1024 = 0x0A,
    RA8875_PWM_CLK_DIV2048 = 0x0B,
    RA8875_PWM_CLK_DIV4096 = 0x0C,
    RA8875_PWM_CLK_DIV8192 = 0x0D,
    RA8875_PWM_CLK_DIV16384 = 0x0E,
    RA8875_PWM_CLK_DIV32768 = 0x0F,
    RA8875_TPCR0 = 0x70,
    RA8875_TPCR0_ENABLE = 0x80,
    RA8875_TPCR0_DISABLE = 0x00,
    RA8875_TPCR0_WAIT_512CLK = 0x00,
    RA8875_TPCR0_WAIT_1024CLK = 0x10,
    RA8875_TPCR0_WAIT_2048CLK = 0x20,
    RA8875_TPCR0_WAIT_4096CLK = 0x30,
    RA8875_TPCR0_WAIT_8192CLK = 0x40,
    RA8875_TPCR0_WAIT_16384CLK = 0x50,
    RA8875_TPCR0_WAIT_32768CLK = 0x60,
    RA8875_TPCR0_WAIT_65536CLK = 0x70,
    RA8875_TPCR0_WAKEENABLE = 0x08,
    RA8875_TPCR0_WAKEDISABLE = 0x00,
    RA8875_TPCR0_ADCCLK_DIV1 = 0x00,
    RA8875_TPCR0_ADCCLK_DIV2 = 0x01,
    RA8875_TPCR0_ADCCLK_DIV4 = 0x02,
    RA8875_TPCR0_ADCCLK_DIV8 = 0x03,
    RA8875_TPCR0_ADCCLK_DIV16 = 0x04,
    RA8875_TPCR0_ADCCLK_DIV32 = 0x05,
    RA8875_TPCR0_ADCCLK_DIV64 = 0x06,
    RA8875_TPCR0_ADCCLK_DIV128 = 0x07,
    RA8875_TPCR1 = 0x71,
    RA8875_TPCR1_AUTO = 0x00,
    RA8875_TPCR1_MANUAL = 0x40,
    RA8875_TPCR1_VREFINT = 0x00,
    RA8875_TPCR1_VREFEXT = 0x20,
    RA8875_TPCR1_DEBOUNCE = 0x04,
    RA8875_TPCR1_NODEBOUNCE = 0x00,
    RA8875_TPCR1_IDLE = 0x00,
    RA8875_TPCR1_WAIT = 0x01,
    RA8875_TPCR1_LATCHX = 0x02,
    RA8875_TPCR1_LATCHY = 0x03,
    RA8875_TPXH = 0x72,
    RA8875_TPYH = 0x73,
    RA8875_TPXYL = 0x74,
    RA8875_INTC1 = 0xF0,
    RA8875_INTC1_KEY = 0x10,
    RA8875_INTC1_DMA = 0x08,
    RA8875_INTC1_TP = 0x04,
    RA8875_INTC1_BTE = 0x02,
    RA8875_INTC2 = 0xF1,
    RA8875_INTC2_KEY = 0x10,
    RA8875_INTC2_DMA = 0x08,
    RA8875_INTC2_TP = 0x04,
    RA8875_INTC2_BTE = 0x02,
    RA8875_SCROLL_BOTH = 0x00,
    RA8875_SCROLL_LAYER1 = 0x40,
    RA8875_SCROLL_LAYER2 = 0x80,
    RA8875_SCROLL_BUFFER = 0xC0,
    RA8875_DMACR = 0xBF,
};

// Hardware Pins
enum
{
    LCD_CS = 10,    // Chip Select goes to Digital 10
    LCD_RESET = 9,  // Chip Reset on Digital 9
    LCD_INT = 3,    // Interrupt for touch (not used)
    LCD_CHIP_ID = 0x8875,
};

void write_data(uint8_t val)
{
    digitalWrite(LCD_CS, LOW);
    act_SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
    act_SPI.transfer(RA8875_DATAWRITE);
    act_SPI.transfer(val);
    act_SPI.endTransaction();
    digitalWrite(LCD_CS, HIGH);
}

uint8_t read_data()
{
    uint8_t ret;
    digitalWrite(LCD_CS, LOW);
    act_SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
    act_SPI.transfer(RA8875_DATAREAD);
    act_SPI.transfer(&ret, 1);
    act_SPI.endTransaction();
    digitalWrite(LCD_CS, HIGH);
    return ret;
}

void write_command(uint8_t com)
{
    digitalWrite(LCD_CS, LOW);
    act_SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
    act_SPI.transfer(RA8875_CMDWRITE);
    act_SPI.transfer(com);
    act_SPI.endTransaction();
    digitalWrite(LCD_CS, HIGH);
}

uint8_t read_status()
{
    uint8_t ret;

    digitalWrite(LCD_CS, LOW);
    act_SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
    act_SPI.transfer(RA8875_CMDREAD);
    act_SPI.transfer(&ret, 1);
    act_SPI.endTransaction();
    digitalWrite(LCD_CS, HIGH);

    return ret;
}

void write_register(uint8_t reg, uint8_t val)
{
    write_command(reg);
    write_data(val);
}

uint8_t read_register(uint8_t reg)
{
    write_command(reg);
    return read_data();
}

void wait_busy(int res)
{
    do
    {
        if (res == 0x01)
            write_command(RA8875_DMACR);  // dma
    } while ((read_status() & res) == res);
}

void wait_poll(uint8_t regname, uint8_t waitflag)
{
    while (1)
    {
        if (!(read_register(regname) & waitflag))
            return;
    }
    return;
}

void set_scroll_area(int x_start, int y_start, int x_stop, int y_stop)
{
    // Horizontal Start point of Scroll Window
    write_command(0x38);
    write_data(x_start);
    write_command(0x39);
    write_data(x_start >> 8);

    // Vertical Start Point of Scroll Window
    write_command(0x3a);
    write_data(y_start);
    write_command(0x3b);
    write_data(y_start >> 8);

    // Horizontal End Point of Scroll Window
    write_command(0x3c);
    write_data(x_stop);
    write_command(0x3d);
    write_data((x_stop) >> 8);

    // Vertical End Point of Scroll Window
    write_command(0x3e);
    write_data(y_stop);
    write_command(0x3f);
    write_data((y_stop) >> 8);

    // Scroll function setting
    write_command(0x52);
    write_data(0x00);
}

void enable_bte(bool on)
{
    uint8_t temp = read_register(RA8875_BECR0);
    on == true ? temp &= ~(1 << 7) : temp |= (1 << 7);
    write_data(temp);
    // write_register(RA8875_BECR0,temp);
}

void move_block(int16_t sx, int16_t sy, int16_t w, int16_t h, int16_t dx, int16_t dy)
{
    uint8_t ROP = 0xC2;  // standard block move

    sx |= 0x8000;
    sy |= 0x8000;

    dx |= 0x8000;
    dy |= 0x8000;

    // source cursor
    write_register(0x54, sx & 0xFF);
    write_register(0x55, sx >> 8);
    write_register(0x56, sy & 0xFF);
    write_register(0x57, sy >> 8);

    // size of move
    write_register(0x5C, w & 0xFF);
    write_register(0x5D, w >> 8);
    write_register(0x5E, h & 0xFF);
    write_register(0x5F, h >> 8);

    // destination cursor
    write_register(0x58, dx & 0xFF);
    write_register(0x59, dx >> 8);
    write_register(0x5A, dy & 0xFF);
    write_register(0x5B, dy >> 8);

    // execute move
    write_register(RA8875_BECR1, ROP);
    write_register(RA8875_BECR0, 0x80);

    // wait to complete
    wait_busy(0x40);
    // delay(25);
}

// this function ignores scanlines and halfdots
void hw_rectangle(int16_t x, int16_t y, int16_t dx, int16_t dy, uint16_t colour)
{
    // set x cursor
    write_command(0x91);
    write_data(x);
    write_command(0x92);
    write_data(x >> 8);

    // set y cursor
    write_command(0x93);
    write_data(y);
    write_command(0x94);
    write_data(y >> 8);

    // set x end
    write_command(0x95);
    write_data(dx - 1);
    write_command(0x96);
    write_data((dx - 1) >> 8);

    // set y end
    write_command(0x97);
    write_data(dy - 1);
    write_command(0x98);
    write_data((dy - 1) >> 8);

    // set colour
    write_command(0x63);
    write_data((colour & 0xf800) >> 11);  // R
    write_command(0x64);
    write_data((colour & 0x07e0) >> 5);  // G
    write_command(0x65);
    write_data((colour & 0x001f));  // B

    // draw the rectangle
    write_command(RA8875_DCR);
    write_data(0xB0);

    // wait for graphics engine to finish
    wait_poll(RA8875_DCR, RA8875_DCR_LINESQUTRI_STATUS);
    // delay(25);
}

// clear a section of the display (uses hw accelerated rectangle)
void clear_section(int sx, int sy, int dx, int dy)
{
    if (!MORELINES)
        sy = sy * 2;
    if (!MORELINES)
        dy = dy * 2;
    hw_rectangle(sx, sy, dx, dy, BACK_BLACK);
}

// write a line of pixels (assumes max width of 8)
void write_byte_line(int x, int y, const unsigned char line /*is MSB first*/, int w, uint16_t front_colour, uint16_t back_colour)
{
    // if we have scanlines then we can't write on an odd line
    if (SCANLINES && !MORELINES && y % 2 != 0)
        return;

    // only do up top 8 wide
    if (w > 8)
        w = 8;

    // create colour average for a held dot
    uint16_t mid_colour = 0;
    if (front_colour == back_colour)
        mid_colour = back_colour;
    else
    {
#if LONGDOTS
        mid_colour = front_colour;
#else
        uint32_t mid_red = (((front_colour & 0xf800) >> 11) + ((back_colour & 0xf800) >> 11)) / 2;
        uint32_t mid_green = (((front_colour & 0x07e0) >> 5) + ((back_colour & 0x07e0) >> 5)) / 2;
        uint32_t mid_blue = (((front_colour & 0x001f) >> 0) + ((back_colour & 0x001f) >> 0)) / 2;
        mid_colour = (uint16_t)((mid_red << 11) & 0xf800) | (uint16_t)((mid_green << 5) & 0x07e0) | (uint16_t)(mid_blue & 0x001f);
#endif
    }

    // set x cursor
    write_register(RA8875_CURH0, x);
    write_register(RA8875_CURH1, x >> 8);

    // set y cursor
    write_register(RA8875_CURV0, y);
    write_register(RA8875_CURV1, y >> 8);

    write_register(RA8875_MWCR0, (read_register(RA8875_MWCR0) & ~RA8875_MWCR0_DIRMASK) | RA8875_MWCR0_LRTD);  // left to right scan
    write_command(RA8875_MRWC);

    // write line of pixels
    act_SPI.beginTransaction(SPISettings(SPISPEED, MSBFIRST, SPI_MODE0));
    digitalWrite(LCD_CS, LOW);
    act_SPI.transfer(RA8875_DATAWRITE);
    for (int p_x = 0; p_x < w; p_x++)
    {
        bool cur = ((line >> (7 - p_x)) & 1);                      // current pixel state
        bool prev = (p_x > 0) && ((line >> (7 - (p_x - 1))) & 1);  // previous pixel state (false if no previous)

        // current pixel is high, and previous was low and we treat this special
        if (PREDOTS && cur && !prev)
            act_SPI.transfer16(mid_colour);

        // current dot is low, but previous was high, write half-way colour
        else if ((HALFDOTS || LONGDOTS) && !cur && prev)
            act_SPI.transfer16(mid_colour);

        // current dot is simply high
        else if (cur)
            act_SPI.transfer16(front_colour);

        // current dot is simply low
        else if (!cur)
            act_SPI.transfer16(back_colour);
    }
    digitalWrite(LCD_CS, HIGH);
    act_SPI.endTransaction();
}

// init the display
void initialise()
{
    // init pins
    pinMode(LCD_CS, OUTPUT);
    digitalWrite(LCD_CS, HIGH);

    pinMode(LCD_RESET, OUTPUT);
    digitalWrite(LCD_RESET, LOW);
    delay(10);  // reset needs to be asserted low. Datasheet suggests 50uS, so we'll say 10ms just to be sure.
    digitalWrite(LCD_RESET, HIGH);

    // init act_SPI
    act_SPI.begin();

#if defined(__SAMD21G18A__)
    pinPeripheral(11, PIO_SERCOM);
    pinPeripheral(12, PIO_SERCOM);
    pinPeripheral(13, PIO_SERCOM);
#endif

    // init RA8875 PLL
    write_register(RA8875_PLLC1, RA8875_PLLC1_PLLDIV1 + 11);
    delay(1);
    write_register(RA8875_PLLC2, RA8875_PLLC2_DIV4);
    delay(1);
    write_register(RA8875_SYSR, RA8875_SYSR_16BPP | RA8875_SYSR_MCU8);

    // set display clock
    write_register(RA8875_PCSR, RA8875_PCSR_PDATL | RA8875_PCSR_2CLK);

    // horizontal sync timing
    uint8_t hoz_sync_start = 32;
    uint8_t hoz_sync_pulse = 96;
    uint8_t hoz_sync_off = 26;
    // vertical sync timing
    uint16_t vert_sync_start = 23;
    uint8_t vert_sync_pulse = 2;
    uint16_t vert_sync_off = 32;

    // Horizontal timing/write settings
    write_register(RA8875_HDWR, (RA8875_WIDTH / 8) - 1);  // width
    write_register(RA8875_HNDFTR, RA8875_HNDFTR_DE_HIGH);
    write_register(RA8875_HNDR, (hoz_sync_off - 2) / 8);                      // off period
    write_register(RA8875_HSTR, hoz_sync_start / 8 - 1);                      // start point
    write_register(RA8875_HPWR, RA8875_HPWR_LOW + (hoz_sync_pulse / 8 - 1));  // pulse width

    // Vertical timing/write settings
    write_register(RA8875_VDHR0, ((uint16_t)RA8875_HEIGHT - 1) & 0xFF);  // height
    write_register(RA8875_VDHR1, ((uint16_t)RA8875_HEIGHT - 1) >> 8);
    write_register(RA8875_VNDR0, vert_sync_off - 1);  // off period
    write_register(RA8875_VNDR1, vert_sync_off >> 8);
    write_register(RA8875_VSTR0, vert_sync_start - 1);  // start point
    write_register(RA8875_VSTR1, vert_sync_start >> 8);
    write_register(RA8875_VPWR, RA8875_VPWR_LOW + vert_sync_pulse - 1);  // pulse width

    // Set X active area
    write_register(RA8875_HSAW0, 0);  // horizontal start
    write_register(RA8875_HSAW1, 0);
    write_register(RA8875_HEAW0, ((uint16_t)RA8875_WIDTH - 1) & 0xFF);  // horizontal end
    write_register(RA8875_HEAW1, ((uint16_t)RA8875_WIDTH - 1) >> 8);

    // Set Y active area
    write_register(RA8875_VSAW0, 0);  // vertical start
    write_register(RA8875_VSAW1, 0);
    write_register(RA8875_VEAW0, ((uint16_t)RA8875_HEIGHT - 1) & 0xFF);  // vertical end
    write_register(RA8875_VEAW1, ((uint16_t)RA8875_HEIGHT - 1) >> 8);

    // clear data in driver chip
    write_register(RA8875_MCLR, RA8875_MCLR_START | RA8875_MCLR_FULL);
    delay(25);

    // Turn on display
    write_register(RA8875_PWRR, RA8875_PWRR_NORMAL | RA8875_PWRR_DISPON);

    // turn on display IO
    write_register(RA8875_GPIOX, 1);

    // Set backlight PWM to always on
    write_register(RA8875_P1CR, RA8875_P1CR_ENABLE | (RA8875_PWM_CLK_DIV1024 & 0x0F));
    write_register(RA8875_P1DCR, 255);

    // clear display
    clear_display();

    // set whole display scrollable
    // set_scroll_area(0, 0, RA8875_WIDTH - 1, RA8875_HEIGHT - 1);

    // set graphics mode (not interested in text mode)
    write_command(RA8875_MWCR0);
    write_data((uint8_t)(read_data() & ~RA8875_MWCR0_TXTMODE));

    // enable BTE
    uint8_t temp = read_register(RA8875_BECR0);
    temp &= ~(1 << 6);
    write_data(temp);
    // done!
}

// clears the driver chip's display ram and then fills it black
void clear_display()
{
    // set it to the background colour
    hw_rectangle(0, 0, RA8875_WIDTH, RA8875_HEIGHT, BACK_BLACK);
}

// fills the display to a colour of your choosing
void fill_display(uint16_t colour)
{
    hw_rectangle(0, 0, RA8875_WIDTH, RA8875_HEIGHT, colour);
}

// assumes all bitmaps are max width of 8 so that they fit in a byte...
void draw_bitmap(int x, int y, const unsigned char bitmap[], int w, int h, uint16_t front_colour, uint16_t back_colour)
{
    if (!MORELINES)
        y = y * 2;
    // for the height of the bitmap, go through each line
    for (int i = 0, l = 0; i < h; i++, l++)
    {
        // write the line twice
        write_byte_line(x, y + l, bitmap[i], w, front_colour, back_colour);

        if (!MORELINES)
        {
            l++;
            write_byte_line(x, y + l, bitmap[i], w, front_colour, back_colour);
        }
    }
}

// max width of 8
void draw_rectangle(int x, int y, int w, int h, uint16_t colour)
{
    if (!MORELINES)
        y = y * 2;

    // for the height of the rectangle, go through each line
    for (int i = 0, l = 0; i < h; i++, l++)
    {
        // write the line twice (the line write function handles scan lines)
        write_byte_line(x, y + h + l, 0xFF, w, colour, BACK_BLACK);

        if (!MORELINES)
        {
            l++;
            write_byte_line(x, y + h + l, 0xFF, w, colour, BACK_BLACK);
        }
    }
}

// set the cursor (doesn't need to do anything....?)
void set_cursor(int x, int y)
{
    if (!MORELINES)
        y = y * 2;
    write_register(RA8875_CURH0, x);
    write_register(RA8875_CURH1, x >> 8);
    write_register(RA8875_CURV0, y);
    write_register(RA8875_CURV1, y >> 8);
}

void scroll_lines(int lines)
{
    if (!MORELINES)
        lines = lines * 2;
    move_block(0, lines, RA8875_WIDTH, RA8875_HEIGHT - lines, 0, 0);
    hw_rectangle(0, RA8875_HEIGHT, RA8875_WIDTH, RA8875_HEIGHT - lines, BACK_BLACK);
}

void sync()
{
    // do nothing
}
};  // namespace graphics
