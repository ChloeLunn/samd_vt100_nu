/*
BSD 3-Clause License

Copyright (c) 2018-2021, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include "graphdrive.hpp"
#include <MCUFRIEND_kbv.h>  // TFT Lib -> note that this bitbangs rather than uses a SERCOM SPI port
#include <stdint.h>

namespace graphics {

enum
{
    LCD_CS = A3,     // Chip Select goes to Analog 3
    LCD_CD = A2,     // Command/Data goes to Analog 2
    LCD_WR = A1,     // LCD Write goes to Analog 1
    LCD_RD = A0,     // LCD Read goes to Analog 0
    LCD_RESET = A4,  // Can alternately just connect to Arduino's reset pin
    LCD_CHIP_ID = 0x9481,
};

MCUFRIEND_kbv tft;

void initialise()
{
    tft.begin(LCD_CHIP_ID);
    tft.setRotation(1);
    clear_display();
}

void clear_display()
{
    tft.fillScreen(BACK_BLACK);
}

void fill_display(uint16_t colour)
{
    tft.fillScreen(BACK_BLACK);
}

void draw_bitmap(int x, int y, const unsigned char bitmap[], int w, int h, uint16_t front_colour, uint16_t back_colour)
{
    tft.drawBitmap(x, y, bitmap, w, h, front_colour, back_colour);
}

void draw_rectangle(int x, int y, int w, int h, uint16_t colour)
{
    // tft.drawRect(x, y, w, h, colour);
    tft.fillRect(x, y, w, h, colour);
}

void clear_section(int sx, int sy, int dx, int dy)
{
    int h = dy - sy;
    if (h < 0)
        h = sy - dy;

    int w = dx - sx;
    if (w < 0)
        w = sx - dx;

    tft.fillRect(sx, sy, w, h, BACK_BLACK);
}

void set_cursor(int x, int y)
{
    tft.setCursor(x, y);
}

// NOT IMPLEMENTED
void scroll_lines(int x, int y)
{
}

void sync()
{
    // do nothing
}

};  // namespace graphics