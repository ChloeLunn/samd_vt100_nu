/*
BSD 3-Clause License

Copyright (c) 2018-2021, Chloe Lunn
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#if !H_GRAPHDRIVE
#define H_GRAPHDRIVE 1

#include <stdint.h>

namespace graphics {

enum  // colours
{
    BLUE = (unsigned short)(0x001F),
    RED = (unsigned short)(0xF800),
    GREEN = (unsigned short)(0x55F0),
    ORANGE = (unsigned short)(0xFC00),
    WHITE = (unsigned short)(0xFFFF),
    CYAN = (unsigned short)(0x07FF),
    MAGENTA = (unsigned short)(0xF81F),
    PURPLE = (unsigned short)(0xF34F),
    YELLOW = (unsigned short)(0xFFE0),
    //_______________________0bRRRRRGGGGGGBBBBB
    BLACK = (unsigned short)(0b0000000000100000),  // Kinda green like the VT100 CRT
    TRUE_BLACK = (unsigned short)(0x0000),         // actual black
    BACK_BLACK = BLACK                             // what's actually used as black
};

void initialise();                                                                                                        // initialise the display
void clear_display();                                                                                                     // clear it back to the BACK_BLACK colour
void fill_display(uint16_t colour);                                                                                       // fill the display with a custom colour
void draw_bitmap(int x, int y, const unsigned char bitmap[], int w, int h, uint16_t front_colour, uint16_t back_colour);  // used for writing characters
void draw_rectangle(int x, int y, int w, int h, uint16_t colour);                                                         // used for writing cursor (sometimes)
void clear_section(int sx, int sy, int dx, int dy);                                                                       // Use to reset a section of the screen back to a fixed colour
void set_cursor(int x, int y);                                                                                            // probably not used/needed...
void scroll_lines(int lines);                                                                                             // scroll the display using the display hardware
void sync();                                                                                                              // sync the display up (used for composite or VGA)
};                                                                                                                        // namespace graphics

#endif