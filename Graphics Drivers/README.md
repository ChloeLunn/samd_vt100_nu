# Graphics Drivers

The code for driving a display is abstracted via the graphdrive.cpp and graphdrive.hpp files.

The .hpp header should need minimal changes, only changing the colour settings *if* different.

The .cpp file can be altered, but must have definitions for the functions declared in the header.

In this way, you can modify the code to run on other displays fairly easily.

Please note however, that the number of characters on the screen is determined by the termopts header as it affects terminal function, you may need to change this for your display to be filled correctly.

The characters are pulled from the VT100 rom and are 7 wide by 9 tall as raw data, and 9 wide and 10 tall including gaps.

If drawn with scanlines enabled, they become approx twice the same height, but note that this is different to double-height formatting options and is instead about display rendering.

TERMWRAP should depend on what you're using it with, but generally you want this ON

## MCUFriend

This display has only been tested with the 3.5" 480x320 ILI9481 display shield you can find cheaply almost everywhere, and uses the "MCUFRIEND_kbv" arduino library because it's faster than the adafruit version and supports more architectures.

### Suggested termopts:

TERMWIDTH = 48 \
TERMHEIGHT = 32 \
TABWIDTH = 4  \
TERMSCROLL = 10 or higher \

Smooth scrolling using the display driver hardware is NOT implemented on this display because it can only do it in portrait, and this is set to be landscape (more cols than rows)

## RA8875

This is a custom coded driver for the RA8875 driver chip used for the large 40-pin displays that don't have their own control chip. (Adafruit make some of these and the driver board)

It only uses the SPI library so should work for any board (this also makes it much more lightweight), but it has only been tested on 800x480 displays. It should work for the 5" and 7" variants of that resolution.

800x480 is the resolution of the original VT100 and allows for closer emulation of the terminal grid, as a result there are three build flags in the graphdrive.cpp file:

**SCANLINES** enables skipping even lines of pixels and leaving them background colours. This is to mimic the 240 scanlines on the 480 dotlines VT100 display (I may implement the control code that allows switching to 480 line mode). Note that this stretches a bitmap, so 5 lines of bitmap becomes 9 lines of display. If you have this disabled, then the "skipped" line is just filled to match the line above.

**LONGDOTS** enables the "dot stretching" used on the VT100. In order to smooth the font, the VT100 wrote the dot at 1.5 times the length needed by the phosphor, which smoothed the right hand side slightly. This is mimicked here by writing an extra pixel after the first that has a colour halfway between the background and forground. This only happens when transitioning between a lit and unlit pixel going left to right on a line.

**MORELINES** Disable vertical stretching and print the characters' heights raw. Allows 48 lines (don't forget to change termopts to match this change!)

### Suggested termopts:

TERMWIDTH = 80 \
TERMHEIGHT = 24 \
TABWIDTH = 8  \
TERMSCROLL = 1 (this enables graphics driver scrolling as line-by-line, which is smoother)